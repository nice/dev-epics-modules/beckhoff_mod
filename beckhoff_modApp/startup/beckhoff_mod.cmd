# Simple IOC to test the Modbus TCP/IP conenction with Beckhoff EK9000     #
#                                                                          #
# created by: Tamas Kerenyi WP12                                           #
# tamas.kerenyi@esss.se                                                    #
#                                                                          #
# **************************************************************************
require(modbus)
require(modbusterminal)
require(beckhoff_mod)

# -----------------------------------------------------------------------------
# Configure ENVIRONMENT
# -----------------------------------------------------------------------------
# Default paths to locate database and protocol
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(beckhoff_mod_DIR)db/")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(beckhoff_mod_DIR)db/")


# -----------------------------------------------------------------------------
# Asyn IP configuration
# -----------------------------------------------------------------------------
drvAsynIPPortConfigure("Bck","192.168.1.1:502",0,0,1)
# -----------------------------------------------------------------------------
# Modbus configuration
modbusInterposeConfig("Bck",0,1000,0)
# -----------------------------------------------------------------------------
#drvModbusAsynConfigure(portName, 
#                       tcpPortName,
#                       slaveAddress, 
#                       modbusFunction, 
#                       modbusStartAddress, 
#                       modbusLength,
#                       dataType,
#                       pollMsec, 
#                       plcType);

# -----------------------------------------------------------------------------
# HoldingRegisterPort
# -----------------------------------------------------------------------------
drvModbusAsynConfigure(HoldingRegisterPort,Bck,0,4,0,4,0,1000,"Beckhoff");
# drvModbusAsynConfigure(HoldingRegisterPort,Bck,0,4,0,32,0,1000,"Beckhoff");

# -----------------------------------------------------------------------------
# WriteMultipleRegisterPort
# -----------------------------------------------------------------------------
drvModbusAsynConfigure(WriteMultipleRegisterPort,Bck,0,16,5120,7,0,1000,"Beckhoff");

# -----------------------------------------------------------------------------
# ReadMultipleRegisterPort
# -----------------------------------------------------------------------------
drvModbusAsynConfigure(ReadMultipleRegisterPort,Bck,0,3,5120,7,0,1000,"Beckhoff");

# -----------------------------------------------------------------------------
# Load record instances
dbLoadRecords("beckhoff_mod.db","P=MOD, PORT=modbus-stream, R=:, A=-1")
