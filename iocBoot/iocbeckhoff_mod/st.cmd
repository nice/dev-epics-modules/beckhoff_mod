#!../../bin/linux-x86_64/beckhoff_mod

## You may have to change beckhoff_mod to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/beckhoff_mod.dbd"
beckhoff_mod_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=tamaskerenyi")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=tamaskerenyi"
